# html2canvas / qrcode

#### 介绍
记录使用html2canvas过程中遇到过的坑，配合使用qrcode生成二维码

1.如果使用背景图片的话，生成的图片会模糊,所以要使用img，并用div包裹
2.配置html2canvas的時候只需要把scale放大两倍就可以了
3.生成海报的区域不能隐藏，否则可能导致生成的是空白的
4.本demo体验地址：http://hb.xiaohuanzi.cn/
